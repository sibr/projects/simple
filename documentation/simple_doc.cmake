# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


set(PROJECT_PAGE "simplePage")
set(PROJECT_LINK "https://gitlab.inria.fr/sibr/projects/simple")
set(PROJECT_DESCRIPTION "A simple sample SIBR project for you to base your projects on")
set(PROJECT_TYPE "TOOLBOX")