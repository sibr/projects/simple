/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#pragma once

# include "Config.hpp"
# include <core/system/Config.hpp>
# include <core/view/ViewBase.hpp>
# include <projects/simple/renderer/SimpleRenderer.hpp>

namespace sibr { 

	/**
	 * \class SimpleView
	 * \brief Wrap a simple renderer.
	 */
	class SIBR_EXP_SIMPLE_EXPORT SimpleView : public sibr::ViewBase
	{
		SIBR_CLASS_PTR(SimpleView);

	public:

		/**
		 * Constructor
		 * \param ibrScene The scene to use for rendering.
		 * \param render_w rendering width
		 * \param render_h rendering height
		 */
		SimpleView(const sibr::BasicIBRScene::Ptr& scene, uint render_w, uint render_h);

		/**
		 * Perform rendering. Called by the view manager or rendering mode.
		 * \param dst The destination rendertarget.
		 * \param eye The novel viewpoint.
		 */
		void onRenderIBR(sibr::IRenderTarget& dst, const sibr::Camera& eye) override;

		/**
		 * Update inputs (do nothing).
		 * \param input The inputs state.
		 */
		void onUpdate(Input& input) override;

		/**
		 * Update the GUI.
		 */
		void onGUI() override;

	private:

		std::shared_ptr<sibr::BasicIBRScene> _scene; ///< The current scene.
		SimpleRenderer::Ptr		_simpleRenderer; ///< The simple renderer.

		RenderTargetRGBA::Ptr	_dstRT; ///< destination RT.
	};

} /*namespace sibr*/ 
