/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef __SIBR_SIMPLE_LIBRARY_CONFIG_HPP__
# define __SIBR_SIMPLE_LIBRARY_CONFIG_HPP__

# include <core/system/Config.hpp>
# include <core/system/CommandLineArgs.hpp>

# ifdef SIBR_OS_WINDOWS
#  ifdef SIBR_STATIC_DEFINE
#    define SIBR_EXPORT
#    define SIBR_NO_EXPORT
#  else
#    ifndef SIBR_EXP_SIMPLE_EXPORT
#      ifdef SIBR_EXP_SIMPLE_EXPORTS
/* We are building this library */
#        define SIBR_EXP_SIMPLE_EXPORT __declspec(dllexport)
#      else
/* We are using this library */
#        define SIBR_EXP_SIMPLE_EXPORT __declspec(dllimport)
#      endif
#    endif
#    ifndef SIBR_NO_EXPORT
#      define SIBR_NO_EXPORT
#    endif
#  endif
# else
#  define SIBR_EXP_SIMPLE_EXPORT
# endif

namespace sibr {

	/// Arguments for all SIBR Simple applications.
	struct SIBRSimpleAppArgs :
		virtual BasicIBRAppArgs {
		Arg<int> integerValue = { "integer", 3, "Integer value" };
		ArgSwitch switchValue = { "switch", false, "Switch value" };
		Arg<bool> booleanValue = { "boolean" , "Boolean value" };
		Arg<std::string> stringValue = { "string" , "String value" };
	};

}

#endif  //__SIBR_SIMPLE_LIBRARY_CONFIG_HPP__