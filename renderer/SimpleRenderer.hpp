/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#pragma once

#include "Config.hpp"

#include <core/scene/BasicIBRScene.hpp>

namespace sibr { 
	
	/**
	 * \class SimpleRenderer
	 * \brief Perform simple rendering 
	 */
	class SIBR_EXP_SIMPLE_EXPORT SimpleRenderer
	{
		SIBR_CLASS_PTR(SimpleRenderer);
	
	public:

		/**
		 * Constructor.
		 * \param cameras The input cameras to use.
		 * \param w The width of the internal rendertargets.
		 * \param h The height of the internal rendertargets.
		 * \param fShader An optional name of the fragment shader to use (default to simple_scene).
		 * \param vShader An optional name of the vertex shader to use (default to simple_scene).
		 * \param facecull Should the mesh be renderer with backface culling.
		 */
		SimpleRenderer(const std::vector<sibr::InputCamera::Ptr> & cameras, 
			const uint w, const uint h,
			const std::string & fShader = "simple/simple_scene",
			const std::string & vShader = "simple/simple_scene",
			const bool facecull = true
		);

		/**
		 * Performs simple rendering
		 */
		virtual void process();
		
	};


} /*namespace sibr*/ 

