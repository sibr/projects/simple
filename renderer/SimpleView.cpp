/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include <projects/simple/renderer/SimpleView.hpp>
#include <core/graphics/GUI.hpp>

sibr::SimpleView::SimpleView(const sibr::BasicIBRScene::Ptr & scene, uint render_w, uint render_h) :
	_scene(scene),
	sibr::ViewBase(render_w, render_h)
{
	const uint w = render_w;
	const uint h = render_h;

	//  Renderers.
	_simpleRenderer.reset(new SimpleRenderer(scene->cameras()->inputCameras(), w, h));

	// Rendertargets.
	_dstRT.reset(new RenderTargetRGBA(w, h, SIBR_CLAMP_UVS));

	// Tell the scene we are a priori using all active cameras.
	std::vector<uint> imgs_ulr;
	const auto & cams = scene->cameras()->inputCameras();
	for (size_t cid = 0; cid < cams.size(); ++cid) {
		if (cams[cid]->isActive()) {
			imgs_ulr.push_back(cid);
		}
	}
	_scene->cameras()->debugFlagCameraAsUsed(imgs_ulr);
}

void sibr::SimpleView::onRenderIBR(sibr::IRenderTarget & dst, const sibr::Camera & eye)
{
	// Perform simple rendering
	_simpleRenderer->process();
}

void sibr::SimpleView::onUpdate(Input & input)
{
}

void sibr::SimpleView::onGUI()
{
	const std::string guiName = "Simple Settings";
	if (ImGui::Begin(guiName.c_str())) {
		ImGui::Text("Hello, world");
	}
	ImGui::End();
}
