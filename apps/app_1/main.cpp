/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include "projects/simple/renderer/SimpleView.hpp"

#include <core/graphics/Window.hpp>
#include <core/view/MultiViewManager.hpp>
#include <core/scene/BasicIBRScene.hpp>
#include <core/raycaster/Raycaster.hpp>
#include <core/view/SceneDebugView.hpp>
#include <core/scene/ParseData.hpp>


#define PROGRAM_NAME "sibr_simple_app"
using namespace sibr;

const char* usage = ""
"Usage: " PROGRAM_NAME " -param <param>"    	                                "\n"
;
struct SimpleAppArgs :
	virtual BasicIBRAppArgs {
};

int main(int ac, char** av)
{
	{
		// Parse Command-line Args
		CommandLineArgs::parseMainArgs(ac, av);
		SimpleAppArgs simpleAppArgs;
		SIBRSimpleAppArgs sibrSimpleAppArgs;
		
		const bool doVSync = !simpleAppArgs.vsync;
		// rendering size
		uint rendering_width = simpleAppArgs.rendering_size.get()[0];
		uint rendering_height = simpleAppArgs.rendering_size.get()[1];
		// window size
		uint win_width = simpleAppArgs.win_width;
		uint win_height = simpleAppArgs.win_height;

		// Window setup
		sibr::Window        window(PROGRAM_NAME, sibr::Vector2i(50, 50), simpleAppArgs);

		// Setup simple IBR Scene
		BasicIBRScene::Ptr		simple_scene(new BasicIBRScene(sibrSimpleAppArgs, true));

		// Fix rendering aspect ratio if user provided rendering size
		uint scene_width = simple_scene->cameras()->inputCameras()[0]->w();
		uint scene_height = simple_scene->cameras()->inputCameras()[0]->h();
		float scene_aspect_ratio = scene_width * 1.0f / scene_height;
		float rendering_aspect_ratio = rendering_width * 1.0f / rendering_height;

		if ((rendering_width > 0)) {
			if (abs(scene_aspect_ratio - rendering_aspect_ratio) > 0.001f) {
				if (scene_width > scene_height) {
					rendering_height = rendering_width / scene_aspect_ratio;
				}
				else {
					rendering_width = rendering_height * scene_aspect_ratio;
				}
			}
		}

		// check rendering size
		rendering_width = (rendering_width <= 0) ? simple_scene->cameras()->inputCameras()[0]->w() : rendering_width;
		rendering_height = (rendering_height <= 0) ? simple_scene->cameras()->inputCameras()[0]->h() : rendering_height;

		// check rendering size
		Vector2u usedResolution(rendering_width, rendering_height);

		// Create the simple view
		SimpleView::Ptr	simpleView(new SimpleView(simple_scene, usedResolution[0], usedResolution[1]));

		// Raycaster.
		std::shared_ptr<sibr::Raycaster> raycaster = std::make_shared<sibr::Raycaster>();
		raycaster->init();
		raycaster->addMesh(simple_scene->proxies()->proxy());

		// Camera handler for main view.
		sibr::InteractiveCameraHandler::Ptr generalCamera(new InteractiveCameraHandler());

		if (!simple_scene->cameras()->inputCameras().empty()) {
			generalCamera->setup(simple_scene->cameras()->inputCameras(), Viewport(0, 0, (float)usedResolution.x(), (float)usedResolution.y()), raycaster);
		} else {
			generalCamera->setup(simple_scene->proxies()->proxy().getBoundingBox(), Viewport(0, 0, (float)usedResolution.x(), (float)usedResolution.y()), raycaster);
		}

		// Add views to mvm.
		MultiViewManager        multiViewManager(window, false);
		// Texture view by default.
		multiViewManager.addIBRSubView("Simple view", simpleView, usedResolution, ImGuiWindowFlags_ResizeFromAnySide);
		multiViewManager.addCameraForView("Simple view", generalCamera);
		// Top view
		const std::shared_ptr<sibr::SceneDebugView>    topView(new sibr::SceneDebugView(simple_scene, generalCamera, simpleAppArgs));
		multiViewManager.addSubView("Top view", topView);
		multiViewManager.getIBRSubView("Top view")->active(false);

		while (window.isOpened())
		{
			sibr::Input::poll();
			window.makeContextCurrent();

			if (sibr::Input::global().key().isActivated(sibr::Key::Escape))
				window.close();

			multiViewManager.onUpdate(sibr::Input::global());
			multiViewManager.onRender(window);

			window.swapBuffer();
		}
	}

	return EXIT_SUCCESS;
}
